**Guide de lancement des scripts**

Avant de suivre ce guide, veuillez vous assurer d'avoir installé les dépendances requises répertoriées dans le fichier `requirements.txt`.

---

**Script de pixel art :**

1. Ouvrez un terminal et déplacez-vous vers l'emplacement du dossier contenant le code et les prérequis.
2. Une fois dans le bon emplacement, saisissez la commande suivante :

   sudo python3 draw.py
 

---

**Script de mémorisation de couleurs :**

1. Ouvrez un terminal et déplacez-vous vers l'emplacement du dossier contenant le code et les prérequis.
2. Une fois dans le bon emplacement, saisissez la commande suivante :
 
   sudo python3 couleurs.py
  

---

**Script du jeu des drapeaux :**

1. Ouvrez un terminal et déplacez-vous vers l'emplacement du dossier contenant le code et les prérequis.
2. Une fois dans le bon emplacement, saisissez la commande suivante :
  
   sudo python3 flag.py
   

---

**Script de l'API météo :**

1. Ouvrez un terminal et déplacez-vous vers l'emplacement du dossier contenant le code et les prérequis.
2. Une fois dans le bon emplacement, saisissez la commande suivante :
 
   sudo python3 api2.py
   

---

Assurez-vous de suivre les instructions de lancement appropriées pour chaque script.
