# -*- coding: utf-8 -*-
"""

@author: baptisteaumont/clementboiteux/emieboncorps
"""


import numpy as np
import time
from random import randint,shuffle
import getpass
from rgb_matrix.rgb_matrix import RGB_Matrix

# Définition des couleurs
colors = {
    'bleu': (0, 85, 164),   
    'blanc': (255, 255, 255),   
    'rouge': (239, 65, 53),
    'vert': (0, 128, 0),
    'noir' : (0,0,0),
    'jaune':(255,233,54),
}

drapeau = {
    'a': 1,   
    'b': 2,   
    'c': 3,
    'd': 4,
    'e': 5,
}


# Initialisation de la matrice de LED
rr = RGB_Matrix(0X74)

# Fonction pour allumer une LED à une position avec une couleur donnée
def draw_color(x, y, color):
    rr.draw_point((x, y), color)   
# Fonction pour effacer toutes les LEDs
def clear_matrix():
    rr.clear()
    rr.display()    

def france_drapeau():
    for i in range(1,7):
        for j in range(1,7):
            if j < 3:
                draw_color(j, i, colors['bleu'])
            elif j < 5:
                draw_color(j, i, colors['blanc'])
            else:
                draw_color(j, i, colors['rouge'])
    rr.display() 
def italie_drapeau():
    for i in range(1,7):
        for j in range(1,7):
            if j < 3:
                draw_color(j, i, colors['vert'])
            elif j < 5:
                draw_color(j, i, colors['blanc'])
            else:
                draw_color(j, i, colors['rouge'])
    rr.display()   
def belgique_drapeau():
    for i in range(1,7):
        for j in range(1,7):
            if j < 3:
                draw_color(j, i, colors['noir'])
            elif j < 5:
                draw_color(j, i, colors['jaune'])
            else:
                draw_color(j, i, colors['rouge'])
    rr.display() 
def autriche_drapeau():
    for i in range(0,8):
        for j in range(1,7):
            if j < 3:
                draw_color(i, j, colors['rouge'])
            elif j < 5:
                draw_color(i, j, colors['blanc'])
            else:
                draw_color(i, j, colors['rouge'])
    rr.display() 
def pologne_drapeau():
    for i in range(1,7):
        for j in range(1,7):
            if j < 4:
                draw_color(i, j, colors['blanc'])
            else:
                draw_color(i, j, colors['rouge'])
    rr.display() 

def drapeau_affiche(numero_drapeau):
    if numero_drapeau == 1:
        return france_drapeau()
    elif numero_drapeau == 2:
        return italie_drapeau()
    elif numero_drapeau == 3:
        return pologne_drapeau()
    elif numero_drapeau == 4:
        return autriche_drapeau()
    else:
        return belgique_drapeau()

# Fonction principale
def main(nbr_questions):
	input("Atention, tu ne verras pas ce que tu ecris... ")
	input("Appuies sur entrée pour commencez ta partie ! ")

	drapeaux = list(drapeau.values())
	shuffle(drapeaux)

	for i in range(nbr_questions):
		numero_drapeau = randint(1,5)
		drapeau_actuel = drapeaux.remove(numero_drapeau)
		drapeau_affiche(numero_drapeau)
					
		user_input = getpass.getpass(prompt="Appuyez sur la lettre correspondant au bon pays:\n a: France \n b: Italie  \n c: Pologne  \n d: Autriche  \n e: Belgique").lower()
		
		# Vérifier si le joueur a deviné la bonne séquence
		if user_input == list(drapeau.keys())[numero_drapeau - 1]:
                        rr.show_text("V", delay=200, color=(0, 255, 0))
		else:
			clear_matrix()
			rr.show_text("F", delay=200, color=(255, 0, 0))
			break

		# Affichage final
		time.sleep(2)
		clear_matrix()

main(3)

