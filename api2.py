# -*- coding: utf-8 -*-
"""

@author: emieboncorps/clementboiteux/baptisteaumont
"""
import requests
from rgb_matrix.rgb_matrix import RGB_Matrix
from rgb_matrix.color import Color
import time
from PIL import Image, ImageDraw
import os
import getpass

user_input = input("Dans qeulle ville souhaitez avoir la météo ? ")
# Fonction pour récupérer la température depuis l'API
def get_temperature():
    url = "https://api.tomorrow.io/v4/weather/realtime?location="+ user_input +"&apikey=LiAz8bjQVzCIo6MZsuS6HYDrgodFIAaT"
    headers = {"accept": "application/json"}
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        temperature = data['data']['values']['temperature']
        return temperature
    else:
        print("Erreur lors de la requête à l'API Tomorrow.io:", response.status_code)
        return None
        
# Fonction pour récupérer le code météo depuis l'API
def get_weather_code():
    url = "https://api.tomorrow.io/v4/weather/realtime?location="+user_input+"&apikey=LiAz8bjQVzCIo6MZsuS6HYDrgodFIAaT"
    headers = {"accept": "application/json"}
    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        weather_code = data['data']['values']['weatherCode']
        return weather_code
    else:
        print("Erreur lors de la requête à l'API Tomorrow.io:", response.status_code)
        return None
        
# Fonction pour afficher la température sur la matrice LED
def display_temperature_on_matrix(temperature):
    rounded_temperature = round(temperature)  # Arrondir la température à l'entier le plus proche
    temperature_str = str(rounded_temperature)
    temperature_str = temperature_str.rjust(2)  # Pour garder la température alignée à droite
    rr = RGB_Matrix(0x74)
    rr.clear()  # Efface la matrice avant d'afficher une nouvelle température
    text = f"{temperature_str}°"
    rr.show_text(text, delay=400, color=(0, 15, 0))  # Affiche la température sur la matrice LED
    rr.display()
    time.sleep(1)  # Affiche la température pendant 5 secondes


rr = RGB_Matrix(0x74)  # Crée un objet RGB_Matrix
col = Color()

# Dessiner un soleil
def soleil():
    sun_color = (255, 255, 0)  # Jaune
    rr.draw_point([3, 1], fill=sun_color)  # Point central
    rr.draw_line([1, 3, 5, 3], fill=sun_color)  # Lignes horizontales
    rr.draw_line([3, 1, 3, 5], fill=sun_color)  # Lignes verticales
    rr.draw_line([1, 1, 5, 5], fill=sun_color)  # Lignes diagonales
    rr.draw_line([5, 1, 1, 5], fill=sun_color)  # Lignes diagonales
    rr.display()
    time.sleep(3)

# Dessiner un nuage
def nuage():
    cloud_color = (100, 100, 100)  # Gris
    rr.draw_ellipse([2, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([5, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([3, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([4, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([6, 3], radius=2, fill=cloud_color)
    rr.display()
    time.sleep(3)
    
def soleil_nuage():
    # Dessiner le demi-soleil à droite
    soleil_color = (255, 255, 0)  # Jaune
    rr.draw_point([7, 1], fill=soleil_color)  # Point central
    rr.draw_line([5, 3, 7, 3], fill=soleil_color)  # Lignes horizontales
    rr.draw_line([7, 1, 7, 5], fill=soleil_color)  # Lignes verticales
    rr.draw_line([5, 1, 5, 5], fill=soleil_color)  # Lignes diagonales
    rr.draw_line([6, 1, 6, 5], fill=soleil_color)  # Lignes diagonales

    # Dessiner le demi-nuage à gauche
    cloud_color = (100, 100, 100)  # Gris
    rr.draw_ellipse([0, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([1, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([2, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([3, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([1, 4], radius=2, fill=cloud_color)
    rr.display()
    time.sleep(3)


def nuage_pluie():
    # Dessiner le nuage
    cloud_color = (100, 100, 100)  # Gris
    rr.draw_ellipse([2, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([3, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([5, 2], radius=2, fill=cloud_color)


    # Dessiner les gouttes de pluie bleues
    blue_color = (0, 0, 255)  # Bleu
    rr.draw_point([2, 5], fill=blue_color)
    rr.draw_point([3, 6], fill=blue_color)
    rr.draw_point([4, 5], fill=blue_color)
    rr.draw_point([5, 6], fill=blue_color)
    rr.draw_point([6, 5], fill=blue_color)
    rr.draw_point([7, 6], fill=blue_color)
    rr.draw_point([1, 7], fill=blue_color)
    rr.draw_point([0, 5], fill=blue_color)
    rr.draw_point([4, 7], fill=blue_color)
    rr.display()
    time.sleep(3)
    
def nuage_neige():
    # Dessiner le nuage
    cloud_color = (100, 100, 100)  # Gris
    rr.draw_ellipse([2, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([3, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([5, 2], radius=2, fill=cloud_color)

    # Dessiner les flocons de neige blancs
    white_color = (255, 255, 255)  # Blanc
    rr.draw_point([2, 5], fill=white_color)
    rr.draw_point([3, 6], fill=white_color)
    rr.draw_point([4, 5], fill=white_color)
    rr.draw_point([5, 6], fill=white_color)
    rr.draw_point([6, 5], fill=white_color)
    rr.draw_point([7, 6], fill=white_color)
    rr.draw_point([1, 7], fill=white_color)
    rr.draw_point([0, 5], fill=white_color)
    rr.draw_point([4, 7], fill=white_color)

    rr.display()
    time.sleep(3)

def nuage_eclair():
    # Dessiner le nuage
    cloud_color = (100, 100, 100)  # Gris
    rr.draw_ellipse([2, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([5, 2], radius=2, fill=cloud_color)
    rr.draw_ellipse([3, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([4, 3], radius=2, fill=cloud_color)
    rr.draw_ellipse([6, 3], radius=2, fill=cloud_color)

    # Dessiner l'éclair orange
    orange_color = (255, 165, 0)  # Orange
    rr.draw_line([2, 2, 4, 4], fill=orange_color, width=2)
    rr.draw_line([3, 1, 5, 3], fill=orange_color, width=2)
    rr.draw_line([4, 2, 6, 4], fill=orange_color, width=2)
    rr.display()
    time.sleep(3)


def display_weather_symbol_on_matrix(weather_code):
    # Récupérer le symbole correspondant au code météo
    if weather_code in [1000]:
        soleil()
    elif weather_code in [1100,1101]:
        soleil_nuage()
    elif weather_code in [1102, 1001,2000,2100]:
        nuage()
    elif weather_code in [4000,4001,4200,4201,6000,6001,6200,6201]:
        nuage_pluie()
    elif weather_code in [5000,5001,5100,5101]:
        nuage_neige()
    elif weather_code in [7000,7101,7102,8000]:
        nuage_eclair()
        
# Récupère la température et l'affiche sur la matrice LED
temperature = get_temperature()
weather_code = get_weather_code()
print(weather_code)
if temperature is not None:
    display_temperature_on_matrix(temperature)
if weather_code is not None:
    display_weather_symbol_on_matrix(weather_code)
