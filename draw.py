# -*- coding: utf-8 -*-
"""

@author: clementboiteux/emieboncorps/baptisteaumont
"""


import tkinter as tk
from rgb_matrix.rgb_matrix import RGB_Matrix

colors = {
    'bleu': (0, 85, 164),   
    'blanc': (255, 255, 255),   
    'rouge': (239, 65, 53),
    'vert': (0, 128, 0),
    'noir' : (0,0,0),
    'jaune':(255,233,54),
}

class DrawingApp:
    # Dimensions de la matrice 8x8
    MATRIX_WIDTH = 8
    MATRIX_HEIGHT = 8
    PIXEL_SIZE = 20
    CANVAS_WIDTH = MATRIX_WIDTH * PIXEL_SIZE
    CANVAS_HEIGHT = MATRIX_HEIGHT * PIXEL_SIZE

    def __init__(self, window):
        self.window = window

        # Initialisation de la matrice de LED
        self.rr = RGB_Matrix(0X74)

        # Création du canevas pour dessiner
        self.canvas = tk.Canvas(window, width=self.CANVAS_WIDTH, height=self.CANVAS_HEIGHT, bg="white")
        self.canvas.pack()

        # Bouton Envoyer
        self.button_send = tk.Button(window, text="Envoyer", command=self.send_drawing)
        self.button_send.pack(side="left", padx=5)
		# Bouton Nettoyer
        self.button_clear = tk.Button(window, text="Nettoyer", command=self.clear_canvas)
        self.button_clear.pack(side="left", padx=5)

        # Initialisation des pixels
        self.pixels = []
        for i in range(self.MATRIX_HEIGHT):
            ligne = []
            for j in range(self.MATRIX_WIDTH):
                ligne.append(None)
            self.pixels.append(ligne)

        self.draw_grid()

        # Bind des événements de dessin
        self.canvas.bind("<Button-1>", self.color_pixel)

    def draw_grid(self):
        # Dessine une grille de carrés sur le canevas
        for i in range(self.MATRIX_HEIGHT):
            for j in range(self.MATRIX_WIDTH):
                x0 = j * self.PIXEL_SIZE
                y0 = i * self.PIXEL_SIZE
                x1 = x0 + self.PIXEL_SIZE
                y1 = y0 + self.PIXEL_SIZE
                self.pixels[i][j] = self.canvas.create_rectangle(x0, y0, x1, y1, fill="white", outline="black")

    def color_pixel(self, event):
        # Trouve le pixel sur lequel l'utilisateur a cliqué et change sa couleur
        x = event.x // self.PIXEL_SIZE
        y = event.y // self.PIXEL_SIZE
        color = "black" if self.canvas.itemcget(self.pixels[y][x], "fill") == "white" else "white"
        self.canvas.itemconfig(self.pixels[y][x], fill=color)

    def send_drawing(self):
        # Efface l'écran de la matrice 8x8
        self.clear_matrix()

        # Parcours des pixels du canevas
        for y in range(self.MATRIX_HEIGHT):
            for x in range(self.MATRIX_WIDTH):
                # Obtient la couleur du pixel du canevas
                color = self.canvas.itemcget(self.pixels[y][x], "fill")
                # Dessine la couleur sur la matrice LED
                self.draw_color(x, y, color)

    def draw_color(self, x, y, color):
        # Fonction pour dessiner un carré de couleur sur la matrice 8x8
        self.rr.draw_point((x, y), color)
        self.rr.display()

    def clear_matrix(self):
        # Fonction pour effacer la matrice 8x8
        self.rr.clear()
        self.rr.display()
        
    def clear_canvas(self):
		# Fonction pour effacer la fenêtre canva
        for y in range(self.MATRIX_HEIGHT):
            for x in range(self.MATRIX_WIDTH):
                self.canvas.itemconfig(self.pixels[y][x], fill="white")

if __name__ == "__main__":
    root = tk.Tk()
    app = DrawingApp(root)
    root.mainloop()
