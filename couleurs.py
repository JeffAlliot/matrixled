# -*- coding: utf-8 -*-
"""

@author: emieboncorps/clementboiteux/baptisteaumont
"""

import time
import random
import getpass
from rgb_matrix.rgb_matrix import RGB_Matrix

# Définition des couleurs
colors = {
    'r': (255, 0, 0),   # Rouge
    'g': (0, 255, 0),   # Vert
    'b': (0, 0, 255),   # Bleu
}

# Initialisation de la matrice de LED
rr = RGB_Matrix(0X74)

# Fonction pour allumer une LED à une position avec une couleur donnée
def draw_color(x, y, color):
    rr.draw_point((x, y), color)

# Fonction pour effacer toutes les LEDs
def clear_matrix():
    rr.clear()
    rr.display()

# Fonction principale
def main():
    max_repetitions = 10

    # Générer une séquence de 10 couleurs
    sequence = [random.choice(list(colors.keys())) for _ in range(max_repetitions)]

    # Attendre que le joueur appuie sur une touche pour commencer
    input("Atention, tu ne verras pas ce que tu ecris... ")
    input("Appuyer sur entrée pour commencer la partie, bonne chance ! ")
    clear_matrix()

    # Boucle principale du jeu
    repetitions = 1
    while repetitions <= max_repetitions:
        # Afficher les couleurs à deviner
        colors_to_guess = sequence[:repetitions]
        for color in colors_to_guess:
            # Choisir une position aléatoire sur la matrice LED pour afficher la couleur
            random_position = (random.randint(0, 7), random.randint(0, 7))
            draw_color(random_position[0], random_position[1], colors[color])
            rr.display()
            time.sleep(1)
            clear_matrix()
            time.sleep(0.5)

        # Attendre que le joueur appuie sur toutes les lettres correspondant à la séquence
        user_input = getpass.getpass(prompt="Appuyez sur les lettres r:red b:blue g:green,correspondant à la séquence : ").lower()

        # Vérifier si le joueur a deviné la bonne séquence
        if user_input == ''.join(colors_to_guess):
            repetitions += 1
        else:
            clear_matrix()
            rr.show_text("Game Over", delay=200, color=(255, 0, 0))
            break

    # Afficher "You Win" si le joueur a réussi 10 séquences
    if repetitions > max_repetitions:
        clear_matrix()
        rr.show_text("You Won", delay=200, color=(0, 255, 0))

    # Affichage final
    time.sleep(2)
    clear_matrix()

if __name__ == "__main__":
    main()
